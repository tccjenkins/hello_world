var express = require('express');
var app = express();
var dynamicModulePath = './dynamicLoad/';

app.get('/', function(req, res) {
    var h = new Homem();
    res.send(h.falar());
});

const fs = require('fs')
const path = require('path')

function loadModules (srcpath,app) {
	var modules = fs.readdirSync(srcpath)
		.filter(file => fs.statSync(path.join(srcpath, file)).isDirectory())
	
	modules.forEach(function (moduleName){
		var module = require(srcpath+moduleName+"/"+moduleName);
		var instance =  module(app);
		console.log("modulo "+moduleName+" carregado!");
	});
}

loadModules(dynamicModulePath,app);


app.listen(3000, function() {
    console.log('Example app listening on port 3000!');
});



















class Pessoa {
    constructor() {

    }
    falar() {
        return "eu sou Uma Pessoa";
    }
}

class Homem extends Pessoa {
    constructor() {
        super();
    }
    falar() {
        return super.falar() + "<BR> eu sou Um HOMEM";
    }

}